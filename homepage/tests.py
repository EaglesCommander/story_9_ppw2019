from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm

import unittest
import time

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from selenium.webdriver.firefox.options import Options

from .views import index

class HomepageFunctionalTest(StaticLiveServerTestCase):

    def setUp(self):
        options = Options()
        options.headless = True
        self.selenium = webdriver.Firefox(options=options)
        self.selenium.implicitly_wait(3)
        super(HomepageFunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(HomepageFunctionalTest, self).tearDown()

    def test_page_works_selenium(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        time.sleep(3)
        self.assertIn('login', self.selenium.page_source)
    
    def test_login_logout_works_selenium(self):
        user = User.objects.create_user('Nico', 'NicoNii@gmail.com', '252521')
        user.set_password('252521')
        user.save()
        
        selenium = self.selenium
        selenium.get(self.live_server_url)

        login_button = selenium.find_element_by_id('login_button')
        login_button.click()

        time.sleep(5)

        username = selenium.find_element_by_id('id_username')
        username.send_keys("Nico")

        password = selenium.find_element_by_id('id_password')
        password.send_keys("252521")

        submit_button = selenium.find_element_by_id('login_button')
        submit_button.click()

        time.sleep(5)

        self.assertIn('Nico', self.selenium.page_source)

        logout_button = selenium.find_element_by_id('logout_button')
        logout_button.click()

        time.sleep(5)

        self.assertIn('login', self.selenium.page_source)

class HomepageUnitTest(TestCase):

    def test_homepage_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)
        
    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_homepage_login(self):
        user = User.objects.create_user('Nico', 'NicoNii@gmail.com', '252521')
        user.set_password('252521')
        user.save()

        client = Client()
        response = client.get('')

        self.assertIn('login', response.content.decode('utf8'))
        
        logged_in = client.login(username='Nico', password='252521')
        self.assertTrue(logged_in)

        response = client.get('')
        self.assertIn('Nico', response.content.decode('utf8'))

    def test_homepage_logout(self):
        user = User.objects.create_user('Nico', 'NicoNii@gmail.com', '252521')
        user.set_password('252521')
        user.save()

        client = Client()
        client.login(username='Nico', password='252521')

        response = client.get('')
        self.assertIn('Nico', response.content.decode('utf8'))

        client.logout()

        response = client.get('')
        self.assertIn('login', response.content.decode('utf8'))


from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect

def index(request):
    response = {'user':request.user}
    return render(request, 'index.html', response)